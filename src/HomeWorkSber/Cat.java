package HomeWorkSber;

public class Cat {
    private String itName;

    public  Cat()
    { itName = "Барсик"; }

    public Cat(String itName)
    {
        this.itName = itName;
    }

    private void sleep()
    {
        System.out.println("Кот " + itName + " Sleep");
    }
    private void meow()
    {
        System.out.println("Кот " + itName + " Meow!");
    }
    private void eat() { System.out.println("Кот " + itName + " Eat");
    }

    public void status()
    {
        int i = 1 + (int) (Math.random()*4);

        switch (i)
        {
            case 1:
                sleep();
                break;
            case 2:
                meow();
                break;
            case 3:
                eat();
                break;
        }
    }

    public static void main(String[] args) {

        Cat c = new Cat();
        Cat d = new Cat("Бегемот");

        c.status();
        d.status();
        d.status();
        c.status();

    }
}

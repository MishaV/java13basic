package Homework;

import java.util.Scanner;

public class HW_12SpecialTaskTwo_StonesForbidden {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        boolean stones = false;
        boolean forbidden = false;

        String mailPackage = input.nextLine();


        if (mailPackage.contains("камни!") &&
                (mailPackage.startsWith("камни!") || mailPackage.endsWith("камни!")))
            stones = true;

        if (mailPackage.contains("запрещенная продукция") &&
                (mailPackage.startsWith("запрещенная продукция") ||
                        mailPackage.endsWith("запрещенная продукция")))
            forbidden = true;

        if (stones && forbidden) System.out.println("в посылке камни и запрещенная продукция");
        else if (stones) System.out.println("камни в посылке");
        else if (forbidden) System.out.println("в посылке запрещенная продукция");
        else System.out.println("все ок");
    }

}




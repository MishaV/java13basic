package Homework;

import java.util.Scanner;

public class HW_129_TrigEqual {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double x = scanner.nextDouble();
        boolean result;

        result = ( ( Math.pow(Math.sin(Math.toRadians(x)),2) + Math.pow(Math.cos(Math.toRadians(x)),2) - 1) == 0 );

        System.out.println(result);

    }
}

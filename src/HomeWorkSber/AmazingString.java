package HomeWorkSber;

import java.util.Scanner;

public class AmazingString {

private char [] str;
    private int lengthStr;    // длинна строки
    private int lengthSub;    // длинна подстроки

    public AmazingString(char[] str)
    {
        int count=0;
        this.str = new char[str.length + 1];
        for(int i=0; i < str.length; i++ ) this.str[i] = str[i];
        this.str[this.str.length-1] = '\0';    // добавим в конец нашего массива char[] str символ '\0'

        for (int i = 0; this.str[i]!='\0'; i++) count++; //посчитаем длинну строки за вычетом спецсимвола '\0'
        lengthStr =count;
    }
    public AmazingString(String str) //перегружаем
    {
        int count=0;
        this.str = new char[str.length() + 1];
        for (int i=0; i < str.length(); i++) this.str[i] = str.charAt(i);
        this.str[this.str.length-1] = '\0';

        for (int i = 0; this.str[i]!='\0'; i++) count++;
        lengthStr =count;
    }
    public String OurString()
    {
        String temp = "";
        for (int i=0; i < lengthStr; i++) temp+=str[i];
        return temp;
    }
    public String OutSymbol(int i)
    {
        String temp="";
        if (i > 0 && i <= lengthStr)
            return "" + str[i-1];
        else
            return "Символа с таким номером нет";
    }
    public int OutLength()
    {
        int count=0;
        for(int i = 0; str[i]!='\0'; i++)
            count++;
        return count;
    }

    public boolean Substring(char[] subStr) {
        int count = 0;
        char subS[] = new char[subStr.length + 1];
        for (int i = 0; i < subStr.length; i++) subS[i] = subStr[i];
        subS[subS.length - 1] = '\0';

        for (int i = 0; subS[i]!='\0'; i++) count++;
        lengthSub = count;                                //длинна подстроки

        count=0;
        for(int i=0; i < lengthStr-1; i++)
        {
            int k = i;
            if(str[i]==subS[0]) {
                for (int j = 0; j < lengthSub; j++) {  //!
                    if (str[k] == subS[j]) count++;
                    k++;
                }
                if (count == lengthSub) return true; //!
                count = 0;
            }
        }
        return false;
    }
    public boolean Substring(String subStr) {
        int count = 0;
        char subS[] = new char[subStr.length() + 1];
        for (int i = 0; i < subStr.length(); i++) subS[i] = subStr.charAt(i);
        subS[subS.length - 1] = '\0';

        for (int i = 0; subS[i]!='\0'; i++) count++;
        lengthSub = count;                                //длинна подстроки

        count=0;
        for(int i=0; i < lengthStr-1; i++)
        {
            int k = i;
            if(str[i]==subS[0]) {
                for (int j = 0; j < lengthSub; j++) {  //!
                    if (str[k] == subS[j]) count++;
                    k++;
                }
                if (count == lengthSub) return true; //!
                count = 0;
            }
        }
        return false;
    }

    public String LeadingSpaces()
    {
        String temp="";
        for(int i = 1; i < lengthStr; i++)
        {
            if(i-1==0 && str[i-1]==' ') continue;
            if (str[i-1]!=' ') temp+=str[i-1];
            if ( str[i-1]==' ' && (str[i-2]!=' ' && str[i]!=' ') ) temp+=str[i-1];
        }
        return temp;
    }
    public String RotateString()
    {
        String temp = "";
        for(int i = lengthStr-1; i >= 0; i--) temp+=str[i];

        return temp;
    }
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        char[] str = {'С', ' ', 'Н', 'о', 'в', 'ы', 'м', ' ', 'г', 'о', 'д', 'о', 'м', '!'};
        String S = "Happy New Year!";
        String leadingSpaces = "            Halloween has come!     ";
        char[] subStr1 = {'Н', 'о', 'в', 'ы', 'м'};
        char[] subStr2 = {'н', 'о', 'в', 'ы', 'м'};
        char[] subStr3 = {'г', 'о', 'д', 'о', 'м', '!'};
        String subS1 = "Year!";
        String subS2 = "new";


        AmazingString S1 = new AmazingString(str);
        AmazingString S2 = new AmazingString(S);
        AmazingString S3 = new AmazingString(leadingSpaces);

        System.out.print("Введите номер символа в строке для отображения: ");
        int i = input.nextInt();

        System.out.println("******************************************************");
        System.out.println("Символ с " + i + "-м номером в строке \"" + S1.OurString() + "\" - '" + S1.OutSymbol(i) + "'");
        System.out.println("Длина строки " + S1.OutLength() + " символов.");
        System.out.println("Перевернутая строка: \"" + S1.RotateString() + "\"");
        System.out.println("******************************************************");
        System.out.println("Символ с " + i + "-м номером в строке \"" + S2.OurString() + "\" - '" + S2.OutSymbol(i) + "'");
        System.out.println("Длина строки " + S2.OutLength() + " символов.");
        System.out.println("Перевернутая строка: \"" + S2.RotateString() + "\"");
        System.out.println("******************************************************");
        System.out.println("Уберем из строки \"" + S3.OurString() + "\" ведущие пробелы.");
        System.out.println("Получаем строку \"" + S3.LeadingSpaces() + "\"");
        System.out.println("******************************************************");
        System.out.println("Содержит ли строка \"" + S1.OurString() + "\" подстроку \"" + OutChar(subStr1) + "\"?");
        System.out.println("Результат: " + S1.Substring(subStr1));
        System.out.println("Содержит ли строка \"" + S1.OurString() + "\" подстроку \"" + OutChar(subStr2) + "\"?");
        System.out.println("Результат: " + S1.Substring(subStr2));
        System.out.println("Содержит ли строка \"" + S1.OurString() + "\" подстроку \"" + OutChar(subStr3) + "\"?");
        System.out.println("Результат: " + S1.Substring(subStr3));
        System.out.println("******************************************************");
        System.out.println("Содержит ли строка \"" + S2.OurString() + "\" подстроку \"" + subS2 + "\"?");
        System.out.println("Результат: " + S2.Substring(subS2));
        System.out.println("Содержит ли строка \"" + S2.OurString() + "\" подстроку \"" + subS1 + "\"?");
        System.out.println("Результат: " + S2.Substring(subS1));
    }
    public static String OutChar(char [] arr) // отображение массива char[] как строки
    {
        String temp = "";
        for (int i=0; i < arr.length; i++) temp+=arr[i];
        return temp;
    }
}
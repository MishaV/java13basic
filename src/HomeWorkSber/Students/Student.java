package HomeWorkSber.Students;

public class Student {
    private String name;          // имя студента
    private String surname;       // фамилия
    private int[] grades;   // оценки за экзамен
    private double averageGrade;


    public String getName() {     // сеттер-геттер для Имени
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {  // сеттер-геттер для фамилии
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {   // сеттер-геттер аттестата
        return grades;
    }
    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void NewGrade(int grade)     // новый бал в конец массива
    {
        for(int i=1; i < grades.length; i++)
        {
            grades[i-1] = grades[i];
        }
        grades[grades.length-1] = grade;
    }

    public double averageGrade()    // средний бал
    {
        double temp=0;

        for(int i=0; i < grades.length; i++)
        {
            temp += grades[i];
        }
        averageGrade = temp / grades.length;
        return averageGrade;
    }

    public double getAverageGrade() {
        return averageGrade;
    }
}

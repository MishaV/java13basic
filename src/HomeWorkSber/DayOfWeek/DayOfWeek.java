package HomeWorkSber.DayOfWeek;

public class DayOfWeek {
    private byte dayNumber;
    private String day;

    public DayOfWeek()
    {
        dayNumber=0;
        day = "";
    }

    public DayOfWeek(byte dayNumber, String day)
    {
        this.dayNumber = dayNumber;
        this.day = day;
    }

    public String getDay() {
        return day;
    }

    public byte getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(byte dayNumber) {
        this.dayNumber = dayNumber;
    }

    public void setDay(String day) {
        this.day = day;
    }
}

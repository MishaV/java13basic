package HomeWorkSber.Students;

public class StudentService {
    public static String bestStudent(Student[] arr)
    {

        double maxAverage = arr[0].getAverageGrade();
        int maxIndex=0;
        for(int i=1; i < arr.length; i++)
        {
            if(maxAverage < arr[i].getAverageGrade())
            {
                maxAverage=arr[i].getAverageGrade();
                maxIndex = i;
            }
        }

        return arr[maxIndex].getName() + " " + arr[maxIndex].getSurname() + " " + String.format("%.2f",maxAverage);
    }
    public static void sortBySurname(Student[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            Student temp = arr[i];
            String firstSur = arr[i].getSurname();
            int fSurIndex = i;

            for (int j = i + 1; j < arr.length; j++) {

                if (firstSur.charAt(0) > (arr[j].getSurname()).charAt(0)) {
                    firstSur = arr[j].getSurname();
                    fSurIndex = j;
                    temp = arr[j];
                } else if (firstSur.charAt(0) == (arr[j].getSurname()).charAt(0)) {
                    int min = Math.min(firstSur.length(), arr[j].getSurname().length());
                    for (int a=1; a < min; a++ )
                    {
                        if (firstSur.charAt(a) > (arr[j].getSurname()).charAt(a)) {
                            firstSur = arr[j].getSurname();
                            fSurIndex = j;
                            temp = arr[j];
                        }

                    }
                }

            }

            // Переставить  если необходимо
            if (fSurIndex != i) {
                arr[fSurIndex] = arr[i];
                arr[i] = temp;
            }
        }

        for (int i = 0; i < arr.length; i++)
        {
            System.out.println("Студент " + arr[i].getName() + " " + arr[i].getSurname() +
                    " " + String.format("%.2f",arr[i].getAverageGrade()));

        }

    }
}

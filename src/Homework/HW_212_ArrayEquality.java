package Homework;
import java.util.Scanner;
public class HW_212_ArrayEquality {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int[] vector1 = new int[n];

        for (int i = 0; i < n; i++) {
            vector1[i] = input.nextInt();
        }

        int m = input.nextInt();
        int[] vector2 = new int[m];

        for (int i = 0; i < m; i++) {
            vector2[i] = input.nextInt();
        }

        if (vector1.length != vector2.length) {
            System.out.println(false);
            return;
        } else {
            for (int i = 0; i < vector2.length; i++)
                if (vector1[i] != vector2[i]) {
                    System.out.println(false);
                    return;
                }
        }

        System.out.println(true);

    }
}
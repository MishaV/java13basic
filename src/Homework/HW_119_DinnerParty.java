package Homework;

import java.util.Scanner;

public class HW_119_DinnerParty {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите бюджет мероприятия, n (0 < n < 100000) : ");
        int n = scanner.nextInt();

        System.out.print("Введите бюджет на одного гостя, k  (0 < k < 1000) : ");
        short k = scanner.nextShort();

        int howMany = n / k;

        System.out.println("На мероприятие можно пригласить " + howMany + " гостей");

    }
}

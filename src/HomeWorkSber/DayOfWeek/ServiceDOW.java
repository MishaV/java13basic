package HomeWorkSber.DayOfWeek;

import java.util.Scanner;

public class ServiceDOW {
    public static void printDOW(DayOfWeek[] arrObj)
    {
        for (int i = 0; i < arrObj.length; i++)
        {
            System.out.println(arrObj[i].getDayNumber() + " " + arrObj[i].getDay());
        }
    }

    public static DayOfWeek[] createArrDOW()
    {
        DayOfWeek[] week = new DayOfWeek[7];

        week[0] = new DayOfWeek((byte) 1, "Monday");
        week[1] = new DayOfWeek((byte)2, "Tuesday");
        week[2] = new DayOfWeek((byte)3, "Wednesday");
        week[3] = new DayOfWeek((byte)4, "Thursday");
        week[4] = new DayOfWeek((byte)5, "Friday");
        week[5] = new DayOfWeek((byte)6, "Saturday");
        week[6] = new DayOfWeek((byte)7, "Sunday");

        return week;
    }

    public static DayOfWeek[] handInput() {

        Scanner input = new Scanner(System.in);
        DayOfWeek[] week = new DayOfWeek[7];

        for (int i = 0; i < week.length; i++) {

            System.out.println("Введите название " + (i+1) + "-ого дня недели: ");

            week[i] = new DayOfWeek();
            week[i].setDayNumber((byte)(i+1));
            week[i].setDay(input.next());
        }

        return week;
    }

    public static void main(String[] args) {

        DayOfWeek[] week;           // объявляем массив

        week = createArrDOW();     // создаем и инициализируем массив объектов

        printDOW(week);             // выводим на экран

        week = handInput();

        printDOW(week);

    }
}

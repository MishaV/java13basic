package HomeWorkSber;

import java.util.Scanner;

public class ATM {
    static int count;                // количество операций
    private String currency;         // тип валюты
    private double exchangeRate;     // курс
//    private int sum;                 // сумма

    public ATM(String currency, double exchangeRate)
    {
        this.currency = currency;
        this.exchangeRate = exchangeRate;
    }

    public static int getCount() {
        return count;
    }

    public double ExchangeRub(int sum) {

        if (!(currency.equals("Rubles"))) {
            System.out.println("Ошибка! Этот метод переводит рубли в доллары.");
            return 0.0;
        } else
        { count++; return sum * exchangeRate; }
    }

    public double ExchangeDoll(int sum) {

        if (!(currency.equals("Dollars"))) {
            System.out.println("Ошибка! Этот метод переводит доллары в рубли.");
            return 0.0;
        } else
        { count++; return sum * exchangeRate; }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int sum;

        do {
            System.out.println("Введите сумму для обмена:");
            sum = input.nextInt();
            if (sum <= 0) {
                System.out.println("Введена нулевая или отрицательная сумма. ");
            }
        } while (sum <= 0);

        ATM oper1 = new ATM("Dollars", 67);              //задаем валюту и курс (1 доллар = 67 руб)
        System.out.println("Переводим в рубли " + sum + " долларов: " +
                oper1.ExchangeDoll(sum) + "руб.");
        System.out.println("Счетчик операций банкомата: " + getCount());  //счетчик операций  - статическая переменная

        ATM oper2 = new ATM("Rubles", 1.0/67);
        System.out.println("Переводим в доллары " + sum + " рублей: " +
                String.format("%.2f",oper2.ExchangeRub(sum)) + "$");
        System.out.println("Счетчик операций банкомата: " + getCount());

    }
}



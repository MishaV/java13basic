package HomeWorkSber;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TimeUnit {

    private byte hour;
    private byte minute;
    private byte second;
    boolean amPm;
    long totalMilliseconds;

    public TimeUnit(byte hour, byte minute, byte second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public TimeUnit(byte hour, byte minute) {
        this.hour = hour;
        this.minute = minute;
        second = 0;
    }

    public TimeUnit(byte hour) {
        this.hour = hour;
        minute = 0;
        second = 0;
    }

    public TimeUnit() {
        hour = 0;
        minute = 0;
        second = 0;
    }

    public void outputTime(Date date, SimpleDateFormat formatDate, String f12_24 )
    {
        if (f12_24.equals("12"))
            formatDate = new SimpleDateFormat("hh:mm:ss a");
        else if(f12_24.equals("24")) formatDate = new SimpleDateFormat("HH:mm:ss");

        totalMilliseconds = System.currentTimeMillis();
        long newDayZeroTime = totalMilliseconds % (24*60*60*1000);
        totalMilliseconds = totalMilliseconds - newDayZeroTime - 3*60*60*1000; ;
        totalMilliseconds += hour*3600000 + minute*60000 + second*1000;
        date.setTime(totalMilliseconds);
        System.out.println(formatDate.format(date));

    }

    public boolean valid(String str, char hms)
    {
        byte temp;

        switch (hms) {
            case 'h':
                try { temp = Byte.parseByte(str.substring(0, 2)); }
                catch (NumberFormatException nfe) {return false;}
                if (temp >= 0 && temp < 24)
                {
                    return true;
                }
                break;
            case 'm':
                try { temp = Byte.parseByte(str.substring(3, 5)); }
                catch (NumberFormatException nfe) {return false;}
                if (temp >= 0 && temp < 60)
                {
                    return true;
                }
                break;
            case 's':
                try { temp = Byte.parseByte(str.substring(6, 8)); }
                catch (NumberFormatException nfe) {return false;}
                if (temp >= 0 && temp < 60)
                {
                    return true;
                }
                break;
            default:
                break;
        }
        return false;
    }

    public boolean plusValidateInputAndFormatTime(String clock){
        if(valid(clock,'h') && valid(clock,'m') && valid(clock,'s'))
        {
            hour += Byte.parseByte(clock.substring(0, 2));
            minute += Byte.parseByte(clock.substring(3, 5));
            second += Byte.parseByte(clock.substring(6, 8));
        }

        return !( valid(clock,'h') && valid(clock,'m') && valid(clock,'s'));
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Date date = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("hh:mm:ss a"); // по умолчанию AM/PM


        String n;                                        // невалидированное время
        TimeUnit clock = new TimeUnit();
        clock.outputTime(date,formatDate,"24");                                    // по умолчанию

        TimeUnit clock3param = new TimeUnit((byte)2, (byte)12, (byte)30);     // 3 параметра
        TimeUnit clock2param = new TimeUnit((byte)7, (byte)48);               // 2
        TimeUnit clock1param = new TimeUnit((byte)15);                        // 1
        clock3param.outputTime(date,formatDate,"12");
        clock2param.outputTime(date,formatDate,"12");
        clock1param.outputTime(date,formatDate,"24");

        do {
            System.out.println("Введите время в формате hh:mm:ss");
            n = input.next();
        }while (clock.plusValidateInputAndFormatTime(n));

        clock.outputTime(date,formatDate,"24");

    }

}



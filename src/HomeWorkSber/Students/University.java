package HomeWorkSber.Students;
import java.util.Scanner;

public class University {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int m;   // число студентов в группе
        int n;   // число оценок конкретного студента

        do {
            System.out.println("Введите число студентов в группе (массиве):");
            m = input.nextInt();
        } while (m < 1);

        Student[] students = new Student[m];

        int k=0;
        while (k < m) {

            do {
                System.out.println("Введите количество оценок " + (k+1) + "-го студента, не более 10: ");
                n = input.nextInt();
            } while (n > 10 || n < 1);

            Student human = new Student();
            int[] arr = new int[n];

            System.out.println("Введите имя студента: ");
            human.setName(input.next());

            System.out.println("Введите фамилию студента: ");
            human.setSurname(input.next());

            for (int i = 0; i < n; i++) {
                System.out.println("Введите " + (i + 1) + "-ю оценку за экзамен");
                arr[i] = input.nextInt();
            }
            human.setGrades(arr);

            System.out.println("Табель экзаменационных оценок:");

            arr = human.getGrades();
            for (int i = 0; i < arr.length; i++) System.out.print(arr[i] + " ");
            System.out.println();

            System.out.println("Введите новую оценку за экзамен.");
            human.NewGrade(input.nextInt());

            System.out.println("Новый табель после ввода:");
            arr = human.getGrades();
            for (int i = 0; i < arr.length; i++) System.out.print(arr[i] + " ");
            System.out.println();


            System.out.println("Студент " + human.getName() + " " + human.getSurname());
            System.out.println("Средний балл оценок за экзамен: " + String.format("%.2f",human.averageGrade()));

            students[k] = human;
            k++;
        }

        StudentService group = new StudentService();

        System.out.println("Лучший студент " + group.bestStudent(students));

        group.sortBySurname(students);

    }
}

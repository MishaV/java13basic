package Homework;

// [java13][ДЗ 2.1] Задача ДОП1 "Генерация пароля"

import java.util.Scanner;

public class HW_2111_CodeGen {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int codeLength;

        do {
            System.out.println("Введите длину генерируемого пароля: ");
            codeLength = input.nextInt();

            if (codeLength < 8) System.out.println("Пароль с " + codeLength + " количеством символов небезопасен!");

        } while (codeLength < 8);

        System.out.println(CryptoTwo(CryptOne(codeLength),codeLength));

    }

    public static int [] CryptOne(int codeL)
    {

        int a = 1;

        int [] numbersOfCharacters = new int[4];

        int random_number1 = a + (int) (Math.random() * codeL/2);
        numbersOfCharacters[0] = random_number1;

        int random_number2 = a + (int) (Math.random() * codeL/3);
        numbersOfCharacters[1] = random_number2;


        int random_number3 = a + (int) (Math.random() * codeL/4);
        numbersOfCharacters[2] = random_number3;


        int random_number4 = codeL - random_number1 - random_number2 - random_number3; // Вычисление 4-го числа
        numbersOfCharacters[3] = random_number4;


        return numbersOfCharacters;

    }
    public static char SpecialSymbol()
    {
        char [] SS = {'-','*','_'};
        return SS[(int) (Math.random() * 3)];
    }
    public static String CryptoTwo(int [] nOCh, int codeL)
    {
        String codeGen = "";
        int n;
        for (int i=0; i<codeL; i++)
        {
            do { n = (int)(Math.random()*4); } while(nOCh[n]==0);


            switch (n)
            {
                case 0:
                    codeGen+= (int) (Math.random()*10);
                    nOCh[0]--;
                    break;

                case 1:
                    codeGen+= (char)('A' + (int) (Math.random()*27));
                    nOCh[1]--;
                    break;

                case 2:
                    codeGen+= (char)('a' + (int) (Math.random()*27));
                    nOCh[2]--;
                    break;

                case 3:
                    codeGen+= SpecialSymbol();
                    nOCh[3]--;
                    break;

                default:
                    break;
            }
        }
        return codeGen;
    }

}

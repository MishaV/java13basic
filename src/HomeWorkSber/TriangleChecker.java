package HomeWorkSber;

public class TriangleChecker {
    /* "Из отрезков можно составить треугольник только в том случае, если их длины удовлетворяют неравенству треугольника,
   т.е. сумма длин двух любых отрезков больше третьего. Очевидно, имеет смысл сравнивать сумму двух меньших отрезков
   с самым большим отрезком." (c) */

    public static boolean TCheck(double a, double b, double c)
    {
        double min = Math.min(Math.min(a,b),c);
        double max = Math.max(Math.max(a,b),c);
        double middle = a + b + c - min - max;

        return (min + middle > max);
    }

    public static void main(String[] args) {

        double a,b,c;

        a=10; b=20; c=30;
        System.out.println("Сторона a= " + a + " сторона b= " + b + " сторона c= " + c +
                " Возможность составить треугольник: " + TCheck(a,b,c));
        a=30; b=40; c=50;
        System.out.println("Сторона a= " + a + " сторона b= " + b + " сторона c= " + c +
                " Возможность составить треугольник: " + TCheck(a,b,c));


    }
}

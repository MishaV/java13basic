package Homework;

// [java13][ДЗ 2.1] Задача10 Игра: "Угадай число"

import java.util.Scanner;

public class HW_2110_Game_GuessTheNumber {
    public static void main(String[] args) {
        GuessTheNumber();
    }

    public static void GuessTheNumber()
    {
        Scanner input = new Scanner(System.in);

        int a = 0;          //begin
        int b = 1001;       //(1000) end inclusive
        int n;

        int m = a + (int)(Math.random() * b);

        System.out.println("Компьютер загадал число. Попробуйте угадать! Любое отрицательное число - [Выход] ");

        do  {
            System.out.println("Введите Ваш вариант числа: ");
            n = input.nextInt();

            if(n < 0) continue;

            if( n == m ) { System.out.println("Победа!"); return; }
            else if ( n > m ) System.out.println("Это число больше загаданного.");
            else System.out.println("Это число меньше загаданного.");

        } while( n > 0 );

    }
}
